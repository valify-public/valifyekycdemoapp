//
//  ViewController.swift
//  ValifyEKYCDemoApp
//
//  Created by Sayed Obaid on 04/04/2022.
//


import UIKit
import ValifyEKYC
import MBProgressHUD

@available(iOS 11.0, *)
class ViewController: UIViewController {
    
    var loadingNotification = MBProgressHUD()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            // Always adopt a light interface style.
            overrideUserInterfaceStyle = .light
        }
        
        valify.sharedInstance.delegate = self
    }
    
    @IBAction func KYC(_ sender: Any) {
        
        let creds = credentials()
        creds.bundle = ""
        creds.userName = ""
        creds.password = ""
        creds.clientID = ""
        creds.clientSecret = ""
        creds.baseURL = ""
        
        let bundle = creds.bundle
        let baseURL = creds.baseURL
        
        getToken(creds: creds) { token in
            self.startSDK(bundle: bundle, token: token!, baseURL: baseURL)
        }
        
        
    }
    
    func startSDK(bundle: String, token: String, baseURL: String){
        DispatchQueue.main.async {
            let language = valifyLanguage.english
            // setting valify settings
            let settings = valifyBuilder()
                .set(bundle: bundle)
                .set(token: token)
                .set(baseURL: baseURL)
                .set(language: language)
            
                .withTerms(true)
                .withTutorial(true)
                .withOCR(true)
                .withDocumentVerification(false)
                .withWatermarkCheck(true)
                .withDataUpdating(true)
                .withLiveness(true)
                .withFaceMatch(true)
                .withRating(true)
                .withoutPermissionsScreen(false)
                .withoutPreviewData(false)
            //                .withCustomLogo(logo: UIImage(named: ""))
            
            //                .withSanctionListCheck(true)
            //                .withPreExistingUserCheck(true)
            
            //                .set(customFaceMatchImage: UIImage(named: ""))
            
                .withLivenessNumOfTrials(livenessNumOfTrials: 5)
                .withLivenessNumOfInstructions(6)
                .withLivenessTimerSeconds(livenessTimerSeconds: 30)
            //                            .withoutSmile(true)
            //                            .withoutLookLeft(true)
            //                            .withoutLookRight(true)
            //                            .withoutCloseEyes(true)
            
                .build()
            
            let valifyRun = valifyFlow.init(withSettings: settings, fromViewController: self)
            do {
                try valifyRun.run()
            } catch {
                print(error)
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    // please note that this is only for quick testing porpose. token should be generated through your backend
    func getToken(creds: credentials, completion: @escaping (_ token: String?) -> ()){
        
        let baseURL = creds.baseURL
        let userName = creds.userName
        let password = creds.password
        let clientID = creds.clientID
        let clientSecret = creds.clientSecret
        
        let header = "\(baseURL)/api/o/token/"
        
        var request  = URLRequest(url: URL(string: header)!)
        request.httpMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        let body = "username=\(userName)&password=\(password)&client_id=\(clientID)&client_secret=\(clientSecret)&grant_type=password".data(using:String.Encoding.ascii, allowLossyConversion: false)
        
        request.httpBody = body
        
        DispatchQueue.main.async {
            self.loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            self.loadingNotification.mode = MBProgressHUDMode.indeterminate
            self.loadingNotification.label.text = "Loading"
        }
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            do {
                
                DispatchQueue.main.async {
                    self.loadingNotification.hide(animated: true)
                }
                
                let httpResponse = response as? HTTPURLResponse
                let statusCode = httpResponse?.statusCode
                print(httpResponse)
                guard let resdata = data else {
                    return
                }
                
                guard let jsonResponse = try JSONSerialization.jsonObject(with: (resdata), options: JSONSerialization.ReadingOptions()) as? NSDictionary else {
                    return
                }
                print(jsonResponse)
                guard let token = jsonResponse.value(forKey: "access_token") as? String else {
                    return
                }
                
                completion(token)
            } catch _ {
                print("not good JSON formatted response")
            }
        }
        task.resume()
    }
    
    
    class credentials {
        var bundle: String = ""
        var userName: String = ""
        var password: String = ""
        var clientID: String = ""
        var clientSecret: String = ""
        var baseURL: String = ""
    }
    
    // your app must support both portrait and landscape modes so that the sdk can functional correctly, you can specify your view controller's orientation as following
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
}

extension ViewController: valifyDelegate {
    
    // Retreving valify results
    func didFinishProcess(_ valifyResponse: valifyResponse, _ valifyToken: String) {
        
        switch valifyResponse {
            
        case .success(let valifyResult):
            
            let firstName = valifyResult.ocrResult?.firstName //string
            let lastName = valifyResult.ocrResult?.lastName //string
            let frontNid = valifyResult.ocrResult?.frontNid //string
            let backNid = valifyResult.ocrResult?.backNid //string
            let street = valifyResult.ocrResult?.street //string
            let area = valifyResult.ocrResult?.area //string
            let gender = valifyResult.ocrResult?.gender //string
            let religion = valifyResult.ocrResult?.religion //string
            let maritalStatus = valifyResult.ocrResult?.maritalStatus //string
            let releaseDate = valifyResult.ocrResult?.releaseDate //string
            let expiration = valifyResult.ocrResult?.expiration //string
            
            
            let updatedProfession = valifyResult.dataUpdatingResult?.updatedProfession //string
            let updatedArea = valifyResult.dataUpdatingResult?.updatedArea //string
            let updatedStreet = valifyResult.dataUpdatingResult?.updatedStreet //string
            
            let frontTransactionId = valifyResult.ocrResult?.frontTransactionId
            let backTransactionId = valifyResult.ocrResult?.backTransactionId
            
            let frontConfidence = valifyResult.ocrResult?.frontConfidence
            let frontDataValidity = valifyResult.ocrResult?.frontDataValidity
            let backDataValidity = valifyResult.ocrResult?.backDataValidity
            
            let idfrontImg = valifyResult.ocrResult?.idfrontImg // image data
            let idBackImg = valifyResult.ocrResult?.idBackImg // image data
            
            let livenessSuccess = valifyResult.livenessResult?.livenessSuccess //bool
            let recognitionSuccess = valifyResult.livenessResult?.recognitionSuccess //bool
            let selfie = valifyResult.livenessResult?.selfie //image data
            
            break;
        case .userExited(let userExited):
            print("userExited")
            let stage = userExited.lastStage // the stage user exited from
            let valifyResult = userExited.valifyResult // results until user exited
            
        case .error(let valifyError):
            print("valifyError")
            let errorCode = valifyError.errorCode
            let valifyResult = valifyError.valifyResult // results until error happened
            print("Error Code:", errorCode)
            
        default:
            break
        }
    }
}
